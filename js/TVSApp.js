var app, sayt;
document.querySelector('#search_field').blur();
Refuel.define('TVSApp',{require: ['GenericModule', 'ListModule', 'DataSource', 'SaytModule','ScrollerModule', 'ajax']},
    function TVSApp() {  
        document.body.focus();
        var sxtitle = document.querySelector('#sx-title');
        var apiUrl = 'http://app.tvshowmark.it/api/v1/';
        var topUrl =    apiUrl+'tvshow/top/';
        var searchUrl = apiUrl+'tvshow/search/';
        var jsearchUrl =apiUrl+'tvshow/jsearch/';
        var userUrl =   apiUrl+'user/';
        var dashboardUrl=apiUrl+'user/dashboard/';

        
        app = Refuel.newModule('GenericModule', {
            root: document.querySelector("body")
            ,autoload: true            
            ,data:  {
                //'top': Refuel.newModule('DataSource', { 
                //    url: 'http://app.tvshowmark.it/api/v1/tvshow/top/', 
                //    timeout: 100,
                //    timeoutCallback: function(e) {console.log('Top timeout error',e);}
                //}),
                'user': Refuel.newModule('DataSource', { url: userUrl, allowedStatus: [403] })
            }
            ,searchSayt: {
                url: jsearchUrl
                ,dataPath: 'results'
                ,delay: 200
                ,minChars: 2
                ,enableKeySelection: false
            }
            ,top: { rowStyle: ['series-row-A', 'series-row-B'] }
            ,dashboard: { rowStyle: ['series-row-A', 'series-row-B'] }
            ,zoom: { 
                seasons: { 
                    rowStyle: ['seasons-row-A', 'seasons-row-B'] 
                } 
            }
        });

        var top = app.items['top'];
        var dashboard = app.items['dashboard'];
        var dashboard_ds = dashboard.dataSource;
        
        function topCallback(e) {
            //IN questa maniera chiunque arrivi con una risposta diventa DS di TOP, non è top a scegliere
            switch(e.url) {
                case topUrl:
                    top.mode = 'top'; 
                    sxtitle.textContent = 'Top of the Week';
                break;
                case searchUrl:
                    top.mode = 'search';  break;
                default:
                    top.mode = 'similar';  
                    sxtitle.textContent = 'Similar to "'+zoom.data.title+'"';
                break;
            }
            if (!e.responseJSON.previous) this.page = 1;
            this.next = e.responseJSON.next ? true : false;
            top.data = this.getData();
        }

        ///  LIST DATASOURCES ///
        var search =  Refuel.newModule('DataSource', { url: searchUrl, dataPath: 'results', successCallback: topCallback
            ,errorCallback: function(e) {
                console.error(e);
                setLoadingList(false);
                setLoadingState(top.root, false);
            }
        });
        var similar =  Refuel.newModule('DataSource', {dataPath: 'results', successCallback: topCallback});
        var top_ds =  Refuel.newModule('DataSource', { url: topUrl, dataPath: 'results',successCallback: topCallback});
        top_ds.load();

        dashboard_ds.setConfig({
            successCallback: function(e) {
                if (!e.responseJSON.previous) dashboard_ds.page = 1;
                dashboard_ds.next = e.responseJSON.next ? true : false;
            }
        });

        var loadingDiv = document.querySelector("#searchProgLoading");
        sayt = app.items['searchSayt'];
        var zoom = app.items['zoom'];
        var currentSeason;
        var dashboardHTMLElement = document.querySelector('#dashboard');

        function loadTop() {
            top.dataSource.load({url: topUrl});
        }
        function clearSayt() {
            sayt.dataSource.abort();
            sayt.elements['inputField'].value = '';
            sayt.dataSource.clear();
            sayt.hide();
        }
        
        sayt.defineAction('clearSayt', function(e) {
            if (this.elements['inputField'].value.length> 1 ) {
                clearSayt();
                loadTop();
            }
        });

        sayt.elements['inputField'].addEventListener('blur', function() {
            document.querySelector('section#search').classList.remove('obscured');
        });
        sayt.elements['inputField'].addEventListener('focus', function() {
            document.querySelector('section#search').classList.add('obscured');
        });
        sayt.elements['inputField'].addEventListener('keypress', function(e) {
            if (e.keyCode == 13) {
                //sayt.dataSource.abort();
                setLoadingState(top.root);
                top.mode = 'newsearch';
                search.currentQuery = this.value.trim();
                sxtitle.textContent = 'Results for "'+search.currentQuery+'"';
                search.load({
                    url: searchUrl,
                    params: 'q='+search.currentQuery, 
                });
                this.blur();
                clearSayt();
            }
        });

        app.subscribe('drawComplete', function() { 
            setLoadingState(app.template.getRoot(), false);
            setSplash(false);
            if(this.data.user.data['is_active']) {
                loadDashboard('watching');
                document.body.classList.add('logged');
            } else {
                document.body.classList.add('unlogged');
            }
            top.scroller = Refuel.newModule('ScrollerModule', {rootId: 'search_list_wrappper', topMargin: 72});
            top.scroller.subscribe('lowerBoundReached', function(e) {
                //quando la lista in top deve fare next... bisogna sapere se è search, top o similar
                if (top.mode === 'top' && top_ds.next) {
                    top_ds.page++;
                    top_ds.load({params: 'page='+top_ds.page, mode: 'add'});
                }
                else if (top.mode == 'search' && search.next) {
                    search.page++;
                    search.load({params: 'q='+search.currentQuery+'&page='+search.page, mode: 'add'});
                }
                else if (top.mode == 'similar' && similar.next) {
                    similar.page++;
                    similar.load({params: 'page='+similar.page, mode: 'add'});
                }
            });
        });

        app.defineAction('returnHome', function(){
            gotoPage('home');
            loadTop();
        });

        zoom.defineAction('info', function(e) {
            if (!e.module.data.overview) return;
            zoom.querySelector('.series-info > .overview').innerHTML = e.module.data.overview;
            zoom.querySelector('.series-info').classList.toggle('hide');
            zoom.scroller.moveTo(0, 10);
        });

        zoom.subscribe('drawComplete', function() {
            setSplash(false);
            document.body.classList.add('zoom');
            setLoadingState(app.template.getRoot(), false);
            if (zoom.scroller) zoom.scroller.destroy();
            zoom.scroller = Refuel.newModule('ScrollerModule', {rootId: 'zoom'});
            zoom.getModule('seasons').applyOnItems(function(season) {
                updateSeason(season);
            });
        });

        dashboard.subscribe('drawComplete', function() {
            setLoadingState(dashboardHTMLElement, false);

            if (dashboard.scroller) dashboard.scroller.destroy();
            dashboard.scroller = Refuel.newModule('ScrollerModule', {rootId: 'dashboard_wrapper', topMargin:109});
            dashboard.scroller.subscribe('lowerBoundReached', function(e) {
                if (dashboard_ds.next) {
                    dashboard_ds.page++;
                    dashboard_ds.load({params: 'page='+dashboard_ds.page, mode: 'add'});
                } 
            });
        });

        zoom.items.seasons.subscribe('loadComplete', function(e) {
            var totalEpisodes = 0;
            for (var i=0, season; season = this.data[i]; i++) {
                for (var n=0, ep; ep = season.episodes[n]; n++) {
                    totalEpisodes++; 
                    if(!ep.aired && ep.first_aired) {
                        var date = convertToDate(ep.first_aired);
                        ep.onAirDate = date.moment;//date.day + '/' + date.month + '/' + date.year;
                    }          
                }
            }
            zoom.data.totalEpisodes = totalEpisodes;
        });

        zoom.items.seasons.subscribe('drawComplete', function() {
            var i=0;
            this.applyOnItems(function(item) {
                expandSeason(item, i === 0);
                i++;
            });
        })

        zoom.items.seasons.defineAction('expandSeason', function(e) {
            var wasclosed = e.module.root.classList.contains('closed');
            
            this.applyOnItems(function(item) {
                expandSeason(item, false);
            });
            var yy = 0;
            if (wasclosed) {
                expandSeason(e.module, true);
                yy = e.currentTarget.offsetTop-50;
            }
            if (zoom.scroller) zoom.scroller.destroy();
            zoom.scroller = Refuel.newModule('ScrollerModule', {rootId: 'zoom'});
            zoom.scroller.moveTo(-yy, 10);
        });

        function expandSeason(item, expand) {
            item.toggleClass('closed', !expand);
            item.toggleClass('opened', expand);
        }

        function updateSeason(season) {
            var nEpisodes = 0;//season.data.episodes.length;
            var nwEpisodes = season.data.user.watched_episodes;

            for (var i = 0, ep; ep = season.data.episodes[i]; i++) {
                if(ep.aired) nEpisodes++;
            }
            season.classList.remove('watched', 'started', 'notairedyet');
            if (!nEpisodes) {
                season.classList.add('notairedyet');
            }
            else if (nEpisodes == nwEpisodes) {
                season.classList.add('watched');
            }
            else if (nwEpisodes > 0) {
                season.classList.add('started');
            }

            updateZoom();
            return nEpisodes == nwEpisodes
        }

        function updateZoom(status) {
            if (!zoom.data.user.watched_episodes && zoom.data.user.status != 'Planned') {
                zoom.data.user.status = null;
            }
            else if ( (zoom.data.totalEpisodes === zoom.data.user.watched_episodes) && zoom.data.status == 'Ended') {
                zoom.data.user.status = 'Completed';
            }
            else if (zoom.data.user.watched_episodes) {
                zoom.data.user.status = 'Watching';
            }
            zoom.root.className = zoom.data.user.status || 'no-state';
            
        }

        function watchEpisode(season, dontupdate) {
            if (!this.data.aired || this.data.user.watched) return null;
            if (!season) season = this.parentModule.parentModule;zoom
            season.data.user.watched_episodes++;
            zoom.data.user.watched_episodes++;
            if (!dontupdate) {
                updateSeason(season);
            } 
            this.data.user.status = 'Watching';
            this.data.user.watched = new Date().getTime();
            return this.data.tvdb_id;
        }

        function unwatchEpisode(season, dontupdate) {
            if (!this.data.aired || !this.data.user.watched) return null;
            if (!season) season = this.parentModule.parentModule;
            season.data.user.watched_episodes--;
            zoom.data.user.watched_episodes--;
            if (!dontupdate) { 
                updateSeason(season);
            }
            this.data.user.watched = null;
            this.data.user.status = null;
            return this.data.tvdb_id;
        }

        zoom.defineAction('watchEpisode', function(e) {
            var action = 'watch';
            if (!e.module.data.user.watched) {
                watchEpisode.call(e.module);
            }
            else {
                unwatchEpisode.call(e.module);
                action = 'unwatch';
            }
            actionOn.call(e.module, action);
        });

        zoom.defineAction('watchSeason', function(e) {
            var action = 'watch';
            //2 methods to know if a series already finished
            //e.module.classList.contains('watched')
            //e.module.data.episodes.length == e.module.data.user.watched_episodes
            if (e.module.classList.contains('watched')) {
                action = 'unwatch';
            } 
            var data = e.module.data;
            var nwatched = data.user.watched_episodes;
            var episodes = e.module.getModule('episodes');
            var idlist = [];
            var season = episodes.parentModule;
            episodes.applyOnItems(function(item) {
                var id = action == 'watch' ? watchEpisode.call(item, season, true) : unwatchEpisode.call(item, season, true);
                if (id) idlist.push(id);
            });
            updateSeason(season);
            actionOn.call(e.module, action, idlist);
        });

        app.defineAction('watch', function(e) {
            e.module.data.user.status = 'Watched';
            actionOn.call(e.module, 'watch');
            e.module.data.user.watched = new Date().getTime();
        });
        app.defineAction('plan', function(e) {
            e.module.data.user.status = 'Planned';
            if (zoom.data && e.module.data.tvdb_id === zoom.data.tvdb_id ) zoom.data.user.status = 'Planned';
            actionOn.call(e.module, 'plan');
        });
        app.defineAction('drop', function(e) {
            e.module.data.user.status = 'Dropped';
            actionOn.call(e.module, 'drop');
        });

        app.defineAction('alertPurge', function(e) {
            document.querySelector('#alert').classList.remove('hide');
        }); 
        app.defineAction('purge', function(e) {
            e.module.data.user.status = 'null';
            actionOn.call(e.module, 'purge');
        });

        app.defineAction('showDashboardWatched', function(e) {
            loadDashboard.call(this, 'watching', e);
        });
        app.defineAction('showDashboardPlanned', function(e) {            
            loadDashboard.call(this, 'planned', e);
        });
        app.defineAction('showDashboardHistory', function(e) {
            //loadDashboard.call(this, 'dropped', e);
            loadDashboard.call(this, 'archived', e);
        });

        app.defineAction('showContext', function(e) {
            e.stopPropagation();
            e.preventDefault();
            e.module.classList.add('showContext');
            setTimeout(function() {
                e.module.classList.remove('showContext');
            } ,5000)
        });
        app.defineAction('hideContext', function(e) {
             e.module.classList.remove('showContext');
        });

        app.defineAction('showZoom', function(e) {
           if(e.module.classList.contains('showContext')) return;
           zoom.dataSource.load({'url': e.module.data['url']});
           similar.load({'url': e.module.data['url']+'similar/'});
           setSplash();
           gotoPage('main');
        });

        function actionOn(action, idlist) {
            var self = this;
            var ids = idlist || [this.data.tvdb_id];

            var csrftoken = Refuel.getCookie('csrftoken');
            var url = userUrl+action+'/';
            var body = {'tvdb_id': ids};
        
            Refuel.ajax.post(url, JSON.stringify(body), {
                headers: {'X-CSRFToken': csrftoken},
                successCallback: function(ev) {
                    setLoadingState(ev.target, false);
                    dashboard.reload();
                },
                errorCallback:  function(ev) {
                    setLoadingState(ev.target, false);
                }
            });
            //console.log('user_status',this.data.user_status);
           // setLoadingState(ev.target, true, '');
        }

        function loadDashboard(section, ev) {
            var url = dashboardUrl+section+'/';
            dashboardHTMLElement.classList.remove(dashboard.currentSection);
            dashboardHTMLElement.classList.add(section);
            if (ev) setLoadingState(dashboardHTMLElement, true, '');
            dashboard.currentSection = section;
            dashboard.dataSource.load({'dataPath':'results','url': url});
        }

        var loader = document.querySelector('.loading-layer');
        var splash = document.querySelector('.splash-layer');

        function setLoadingState(element, active, msg) {
            if (active === undefined) active = true;
            if (loader.parentNode) loader.parentNode.removeChild(loader);
            if (msg === undefined) msg = '...loading wait...';
            loader.querySelector('.wait-message').innerHTML = msg;

            if (element) element.appendChild(loader);
            active ? loader.classList.remove('hide') : loader.classList.add('hide');
        }

        function setLoadingList(active) {    
            if (active === undefined) active = true;        
            setLoadingState(loadingDiv, active, '');
            active ? loadingDiv.classList.remove('hide') : loadingDiv.classList.add('hide');
        }

        function setSplash(active) {
            if (active === undefined) active = true;
            active ? splash.classList.remove('hide') : splash.classList.add('hide');
        }

        function convertToDate(timestamp) {
            var date = new Date(timestamp * 1000);
            var obj = { 
                year: date.getFullYear(),
                month: date.getMonth()+1,
                day: date.getDate(),
                hh: date.getHours(),
                mm: date.getMinutes()
            };
            moment.lang('en', {
                calendar : {
                    lastDay : '[Yesterday at] LT',
                    sameDay : '[Today at] LT',
                    nextDay : '[Tomorrow at] LT',
                    lastWeek : '[last] dddd',
                    nextWeek : '[next] dddd',
                    sameElse : 'L'
                }
            });
            obj.moment = moment(date).calendar();//fromNow();
            return obj;
        }

    }
);