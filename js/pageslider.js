/** TODO

- Tab search si apre di 100% - 48px
- post resize si sminkiano le dimensioni delle transitions

**/

/**
     * requestAnimationFrame and cancel polyfill
     */
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame =
                window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
                    timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function Carousel(element)
{
    var self = this;
    element = $(element);

    var container = $(">ul", element);
    var panes = $(">ul>*", element);

    var pane_width = 0;
    var pane_count = panes.length;
    var elements = [];
    this.elements = elements;
    var currentSectionIndex = 0;

    this.init = function() {
        elements = [];
        setPaneDimensions();
    };

    /**
     * set the pane dimensions and scale the container
     */
    function setPaneDimensions() {
        var totalWidth = 0;
        //var mainWidth = element.width(); 
        var mainWidth = (window.innerWidth > 0) ? window.innerWidth : screen.width;
        
        $('#zoom').width(mainWidth);
        panes.each(function() {
            var margin_right = parseInt(this.getAttribute("data-rf-margin-right")) || 0;
            var margin_left = parseInt(this.getAttribute('data-rf-margin-left')) || 0;
            var width = mainWidth - (margin_right || margin_left || 0);
            $(this).width(width);
            elements.push({ 'domElement': this, 
                            'width': width, 
                            'x': totalWidth - margin_left,
                            'name': this.id
                        });
            
            totalWidth += width;

            if (this.hasAttribute('data-rf-default-section')) defaultSection = this.id;
        });
        container.width(totalWidth);
        self.showPaneByName(defaultSection);
    };

    this.disablePane = function(index) {
        if (elements[index]) elements[index].disabled = true;
    }
    this.disablePaneByName = function(name) {
        for (var i=0, el; el = elements[i]; i++) {
            if(el.name == name) {
                this.disablePane(i);
            }
        }
    }

    this.showPaneByName = function( name ) {
        for (var i=0, el; el = elements[i]; i++) {
            if(el.name == name) {
                this.showPane(i);
            }
        }
    };

    /**
     * show pane by index
     * @param   {Number}    index
     */
    this.showPane = function( index ) {
        var lastPanel = elements[currentSectionIndex];
        var lastSection = currentSection;
        var selectedPanel = elements[index];
        if (selectedPanel.disabled) return;
        
        currentSectionIndex = index;
        
        currentSection = selectedPanel.name;
        var offset = selectedPanel.x;
        setContainerOffset(offset*-1, true);
        if (lastSection) document.body.classList.remove(lastSection);
        document.body.classList.add(currentSection);
        if (lastPanel) lastPanel.domElement.classList.remove('current');
        selectedPanel.domElement.classList.add('current');
    };

    function setContainerOffset(percent, animate) {
        container.removeClass("animate");
        if(animate) {
            container.addClass("animate");
        }
        if(Modernizr.csstransforms3d) {
            container.css("transform", "translate3d("+ percent +"px,0,0) scale3d(1,1,1)");
        }
        else if(Modernizr.csstransforms) {
            container.css("transform", "translate("+ percent +"px,0)");
        }
        else {
            container.css("left", percent+"px");
        }
    }

    this.next = function() { return this.showPane(currentSectionIndex+1, true); };
    this.prev = function() { return this.showPane(currentSectionIndex-1, true); };

    function handleHammer(ev) {
        ev.gesture.preventDefault();
        switch(ev.type) {
            case 'dragright':
            case 'dragleft':
                // stick to the finger
                var pane_offset = -(100/pane_count)*currentSectionIndex;
                var drag_offset = ((100/pane_width)*ev.gesture.deltaX) / pane_count;

                // slow down at the first and last pane
                if((currentSectionIndex == 0 && ev.gesture.direction == Hammer.DIRECTION_RIGHT) ||
                    (currentSectionIndex == pane_count-1 && ev.gesture.direction == Hammer.DIRECTION_LEFT)) {
                    drag_offset *= .4;
                }
                setContainerOffset(drag_offset + pane_offset);
                break;

            case 'swipeleft':
                self.next();
                ev.gesture.stopDetect();
                break;

            case 'swiperight':
                self.prev();
                ev.gesture.stopDetect();
                break;

            case 'release':
                // more then 50% moved, navigate
                return;
                if(Math.abs(ev.gesture.deltaX) > pane_width/2) {
                    if(ev.gesture.direction == 'right') {
                        self.prev();
                    } else {
                        self.next();
                    }
                }
                else {
                    self.showPane(currentSectionIndex, true);
                }
                break;
        }
    }
    Hammer(element[0], { drag_lock_to_axis: true }).on("release dragleft dragright swipeleft swiperight", handleHammer);
}


var carousel = new Carousel("#carousel");
var gotobuttons = document.querySelectorAll('[data-rf-goto]');

var currentSection, defaultSection, requestedSection;
var allowResize = false;
var lastWidth = null;
var lastHeight = null;
for (var i=0, but; but = gotobuttons[i]; i++) {
    but.addEventListener('click', function(e) {
        var section = this.getAttribute('data-rf-goto');
        gotoPage(section);
    });
}
/*
function gotoPage(section) {
    if (section == 'home') document.body.classList.remove('zoom');
    if (section == currentSection) 
        carousel.showPaneByName(defaultSection);
    else 
        carousel.showPaneByName(section); 
}
*/
function gotoPage(section) {
    if (section == 'home') document.body.classList.remove('zoom');
    if (section == currentSection) 
        requestedSection = defaultSection;
    else 
        requestedSection = section;

    document.location.hash = '/'+requestedSection;
}


Path.map("#/:name").to(function() {
    carousel.showPaneByName(this.params['name']);
});
Path.listen();

function loadHandler(e) {
    allowResize = true;
    setTimeout(function() {allowResize=false;}, 1000);
}
function orientationHandler(e) {
    allowResize = true;        
}
function resizeHandler(e) {
    //console.log('resize '+allowResize);
    if (allowResize) {
        carousel.init();
        allowResize = false;
    }
}

window.addEventListener('resize', resizeHandler);
window.addEventListener('DOMContentLoaded', loadHandler);
window.addEventListener('orientationchange', orientationHandler);
carousel.init();