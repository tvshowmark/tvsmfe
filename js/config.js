Refuel = {
	config: {
		basePath: '/js/refueljs',
		requireFilePath: '/js/refueljs/lib/require.min.js',
		libs: {
			Path: '/js/refueljs/lib/path.min.js',
			Hammer: '/js/refueljs/lib/hammer.min.js',
			polyfills: '/js/refueljs/lib/polyfills.min.js'
		},
		autoObserve: true
	}
};